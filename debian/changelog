fonts-inter (4.1+ds-1+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Mar 2025 17:29:32 +0000

fonts-inter (4.1+ds-1) unstable; urgency=medium

  * New upstream version. (Closes: #1093547)

 -- Alex Myczko <tar@debian.org>  Tue, 18 Feb 2025 11:48:54 +0000

fonts-inter (4.0+ds-2) unstable; urgency=medium

  * Bump standards version to 4.7.0.

 -- Alex Myczko <tar@debian.org>  Tue, 23 Jul 2024 16:09:13 +0000

fonts-inter (4.0+ds-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Fri, 01 Dec 2023 20:41:05 +0100

fonts-inter (4.0~beta9+ds-1) experimental; urgency=medium

  * New upstream version.
  * d/copyright: bump years.
  * Bump standards version to 4.6.2.

 -- Gürkan Myczko <tar@debian.org>  Sat, 27 May 2023 16:15:19 +0200

fonts-inter (4.0~beta8+ds-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Sat, 08 Apr 2023 22:10:05 +0200

fonts-inter (4.0~beta7+ds-1+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Tue, 25 Apr 2023 10:33:23 +0530

fonts-inter (4.0~beta7+ds-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Wed, 26 Oct 2022 16:33:51 +0200

fonts-inter (4.0~beta6+ds-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Tue, 18 Oct 2022 10:24:26 +0200

fonts-inter (4.0~beta5+ds-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Mon, 10 Oct 2022 07:10:34 +0200

fonts-inter (4.0~beta1+ds-2) unstable; urgency=medium

  * Upload to sid.

 -- Gürkan Myczko <tar@debian.org>  Sun, 02 Oct 2022 19:59:05 +0200

fonts-inter (4.0~beta1+ds-1) experimental; urgency=medium

  * New upstream version. (Closes: #1017286)
  * Switch to upstream prebuilt fonts due to unavailable
    building tools of upstream.
  * Bump standards version to 4.6.1.
  * Update maintainer email.
  * d/copyright: bump years.
  * d/*.install: updated paths.
  * d/rules: skip building.

 -- Gürkan Myczko <tar@debian.org>  Fri, 30 Sep 2022 15:02:43 +0200

fonts-inter (3.19+ds-2) unstable; urgency=medium

  * Upload to unstable.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sun, 15 Aug 2021 11:46:16 +0200

fonts-inter (3.19+ds-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sun, 20 Jun 2021 21:50:33 +0200

fonts-inter (3.18+ds-1) experimental; urgency=medium

  * New upstream version.
  * Bump standards version to 4.5.1.
  * d/watch: added filemangle for repacking.
  * Drop unused lintian override.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 01 Apr 2021 08:23:23 +0200

fonts-inter (3.15+ds-3) unstable; urgency=medium

  * d/rules: Patch adding --expand-features-to-instances for fontmake.
    Thanks Bart Martens. (Closes: #978365)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 23 Feb 2021 06:41:28 +0100

fonts-inter (3.15+ds-2) unstable; urgency=medium

  * Source only upload. (Closes: #975474)
  * Thanks for fixing AppStream, applied patch of Michel Le Bihan.
    (Closes: #968898)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 23 Nov 2020 08:37:47 +0100

fonts-inter (3.15+ds-1) unstable; urgency=medium

  * New upstream version.
  * Bump debhelper version to 13.
  * Added upstream metadata file.
  * Also build VF font. (Closes: #968936)
  * d/control:
    - added Rules-Requires-Root.
    - deduplicate short description.
  * moved d/s.l-o to d/s/lintian-overrides.
  * d/patches: dropped.
  * d/copyright:
    - updated years.
    - added Files-Excluded.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 26 Aug 2020 08:08:41 +0200

fonts-inter (3.12-1) unstable; urgency=medium

  * New upstream version.
  * Many thanks for the patch Yao Wei. (Closes: #950099)
  * Added ufo sources.
  * Bump standards version to 4.5.0.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sun, 09 Feb 2020 14:50:57 +0100

fonts-inter (3.11-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.4.1.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 23 Oct 2019 08:37:44 +0200

fonts-inter (3.10-1) unstable; urgency=medium

  * New upstream version.
  * d/control: add Vcs fields.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 12 Sep 2019 13:08:58 +0200

fonts-inter (3.7-2) unstable; urgency=medium

  * Bump standards version to 4.4.0.
  * Add AppStream metadata.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 06 Sep 2019 21:26:41 +0200

fonts-inter (3.7-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 06 Jun 2019 15:03:28 +0200

fonts-inter (3.3-1) unstable; urgency=medium

  * Initial release. (Closes: #921504)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 05 Feb 2019 09:57:45 +0100
